
Project Review
=====================================================

## Overview

1) Briefly summarize the experiment in this project.
The project discusses about data transfer using TCP and UDP. About how it differs the throughput by varying the bandwidth and delay. The authors have presented four cases having different bandwidths and delays. Three different packet sizes are also taken into consideration. Quantitative analysis has been in the form of graphs to conclude the results of the project.

2) Does the project generally follow the guidelines and parameters we have
learned in class?
The parameters used are Bandwidth, Latency and Packet size
The metric used is Throughput.  
So, yes they have used the parameters we have already implemented in the previous Geni Labs. They have varied these parameters so as to show the variation in the Throughput.




## Experiment design

1) What is the goal of this experiment? Is it a focused, specific goal?
Is it useful and likely to have interesting results?
The goal is to observe the throughput variation when the parameters are changed. Whether the throughput is greater for a TCP packet or a UDP packet. I think it is a good goal having a distinct conclusion However, they have used  a lot of parameters which makes the study a little less efficient and would take a longer time to reproduce this project.

2) Are the metric(s) and parameters chosen appropriate for this
experiment goal? Does the experiment design clearly support the experiment goal?
They are apt because if a data transfer through a tcp and udp packet is to be compared then their throughput should be the primary focus. Which they have successfully measured.

3) Is the experiment well designed to obtain maximum information with the
minimum number of trials?
The experiment obtains the maximum information but they have presented four different cases to do so. They are certainly feasible to perform but would take a longer time to get to the bottom conclusion of the experiment.





4) Are the metrics selected for study the *right* metrics? Are they clear,
unambiguous, and likely to lead to correct conclusions? Are there other
metrics that might have been better suited for this experiment?
The metric being used is Throughput. They could have also used congestion window as their metric so as to measure the packet loss or speed of transmission by varying the congestion window size. It would distinctively show the packet loss in tcp and udp packets.

5) Are the parameters of the experiment meaningful? Are the ranges
over which parameters vary meaningful and representative?
The four cases being considered are as follows:
1.	Bandwidth: 100Mbps, Latency: 0ms
2.	Bandwidth: 100Mbps, Latency: 50ms
3.	Bandwidth: 1Gbps, Latency: 0ms
4.	Bandwidth: 1Gbps, Latency: 50ms
These cases have been considered for different packet loss rates, viz., 0.0001, 0.001, 0.005. The parameters are meaningful because they help in analyzing their metric that is the throughput. They have represented it using R programming to plot a graph which shows their result for tcp as well as udp.

6) Have the authors sufficiently addressed the possibility of interactions
between parameters?
All the parameters being used have been varied in the cases being considered to study their interactions. Also, throughput varies for every case. Hence, the interaction between the parameters has produced different result which helps analyze the experiment in depth.


7) Are comparisons made reasonably? Is the baseline selected for comparison appropriate
and realistic?
The baseline is clear. Measuring the throughput on the basis of the considered parameters.

## Communicating results


1) Do the authors report the quantitative results of their experiment?
The throughputs from the above cases are compared for UDP and TCP transfer. The raw data obtained from the above experiment is visualized using R studio. The generated linear graphs shows the study of UDP and TCP packet flow by evaluating the throughput in both the cases with respect to varying bandwidth, latency and packet size. Hence, their quantitative analysis is clear and represents their results in a user-friendly manner.

2) Is there information given about the variation and/or distribution of
experimental results?
Yes they have given the steps which tell me how to vary the throughput by varying the parameters using R studio.

3) Do the authors practice *data integrity* - telling the truth about their data,
avoiding ratio games and other practices to artificially make their results seem better?
Yes they do follow data integrity. As we can see the results being produced in a step-wise manner using the instructions being provided.

4) Is the data presented in a clear and effective way? If the data is presented in
graphical form, is the type of graph selected appropriate for the "story" that
the data is telling?
The data result is represented in a graphic form, however the the graph plotted is a little hard to understand as the parameters overlap each other making it difficult to read.

5) Are the conclusions drawn by the authors sufficiently supported by the
experiment results?
Yes, the conclusion states that the effect of Packet size on throughput is observed as follows:
TCP: For constant Bandwidth and Delay, when we change packet size of the data, throughput remains constant. UDP: For constant Bandwidth and Delay, when we change packet size of the data, throughput remains constant.
b) Effect of Bandwidth on throughput:
TCP: For constant latency and packet size, when we increase the bandwidth, throughput also increases UDP: No observable changes.
c) Effect of latency on throughput:
TCP: For constant bandwidth and packet size, when we increase the latency, throughput decreases. UDP: No observable change.
The showcased result has demonstrated that TCP throughputs significantly sffers in high Bandwidth networks especially with long delays, and the impact of loss was relatively less critical than delay on the TCP throughput.

TCP: For constant bandwith, latency and packet size ,when we change the packet loss rate throughput decreases as the packet loss rate increases. UDP: No observable change.
Hence, through their conclusion we see that, their graphical analysis compare the results.

## Reproducible research



1) Did the authors include instructions for reproducing the experiment in 3 ways (Raw data -> Results,
Existing experiment setup -> Data, and Set up experiment)? Are the instructions clear
and easy to understand?
Yes they have included the Rspec file, Raw data, steps to analyze how to perform the graphical analysis using R studio. So, the experiment is pretty much reproducible.


2) Were you able to successfully produce experiment results?
Yes, I was able to successfully produce the experiment results but it did take a day to finish the entire experiment including the understanding part.

3) How long did it take you to run this experiment, from start to finish?
It took me a day to perform and get the results. Which includes reading their proposal, report and analyzing their results.
4) Did you need to make any changes or do any additional steps beyond the documentation in order to successfully complete this experiment? Describe *in detail*. How long did these extra steps or changes take to figure out?
The experiment was long in itself. Meaning if I would have added additional steps like using congestion window as another metric, it would take more time and make it less efficient.

5) In the [lecture on reproducible experiments](http://witestlab.poly.edu/~ffund/el6383/files/Reproducible+experiments.pdf), we mentioned six degrees of reproducibility. How would you characterize this experiment - where does it fall on the six degrees of reproducibility?
It can be a degree 5 experiment as they have clearly performed what they have stated in their goal without confusing me. Their results are well put and can be analyzed successfully.


## Other comments to authors

Please write any other comments that you think might help the authors
of this project improve their experiment.
The experiment as a whole was indeed very good. They could have used less parameters for better efficiency and reduce the time for reproducing the experiment.
