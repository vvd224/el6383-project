
Project Review
=====================================================


## Overview

1) Briefly summarize the experiment in this project.

  My project for reviewing is to compare the preformance of TCP and UDP in a network
  by varying bandwidth, delays and packet loss rate.

2) Does the project generally follow the guidelines and parameters we have
learned in class?

  Yes, I think the project follow the guidelines we leraned very well. By keeping one
  parameters same and changing others, it is a good method. Totally it's a good design.


## Experiment design

1) What is the goal of this experiment? Is it a focused, specific goal?
Is it useful and likely to have interesting results?

  The goal of our experiment is to understand the throughput behaviour of UDP and TCP by varying
  packet sizes and link parameters like bandwidth, latency and packet loss rates.

  It's a focused and specific goal and it's useful and definitely have interesting rsults for me.

2) Are the metric(s) and parameters chosen appropriate for this
experiment goal? Does the experiment design clearly support the experiment goal?

  Yes. Bandwidth, delays and packet loss are the first parameters we come up with when doing this kind of experiment.
  The packet size is also a parameter in this experiment.
  Throughput is a good metric for evaluating the performance of a network.
  The goal is to understand the throughput behavior, so the design support the goal.

3) Is the experiment well designed to obtain maximum information with the
minimum number of trials?

  Yes, by comparing respectivly in the first part, we can get the throughput in different cases. Each case  with 3 different
  sizes packets is the minimum number of trails I can think, or the experiment will be lucking evidence to prove the conclusion.

4) Are the metrics selected for study the *right* metrics? Are they clear,
unambiguous, and likely to lead to correct conclusions? Are there other
metrics that might have been better suited for this experiment?

  Yes, throughput is the right metric. It is clear to lead the correct conclusion since throughput is a good way to
  demonstrate performance of a network.

  I would like to say that TTL is probably another metric for this experiment but since the goal is to see the throughput bahavior,
  throughput itself is the best.

5) Are the parameters of the experiment meaningful? Are the ranges
over which parameters vary meaningful and representative?

  Yes, they are meaningful. These parameters show common cases in real time transmitting.
  I think the packet size is a little bit smaller than a good range. Everything is good beside this.
  Both of them are representive.

6) Have the authors sufficiently addressed the possibility of interactions
between parameters?

  I didn't see any address of interactions possibility. The author assumes that they are independent.

7) Are comparisons made reasonably? Is the baseline selected for comparison appropriate
and realistic?

  Yes, they are resonable. They are comparing metric under one fixed parameter.
  They baseline is appropriate and realistic.

## Communicating results


1) Do the authors report the quantitative results of their experiment?

  Yes, he made some plots at the end.

2) Is there information given about the variation and/or distribution of
experimental results?

  Yes, the author has 2 bar graphs of confidence intervals.

3) Do the authors practice *data integrity* - telling the truth about their data,
avoiding ratio games and other practices to artificially make their results seem better?

  Yes, the author are ploting the graphs based on their iperf results and doing same trails of the experiments.

4) Is the data presented in a clear and effective way? If the data is presented in
graphical form, is the type of graph selected appropriate for the "story" that
the data is telling?

  Well, I would say that the graph is clear but not clear enough, and the graph is not effective. We can calculate
  the means of all the intervals since we don't need the data of every single moment, we just need to see thier overall
  performance.

5) Are the conclusions drawn by the authors sufficiently supported by the
experiment results?

  Yes, the conclusion that 'TCP throughputs significantly suffers in high Bandwidth networks especially with long delays,
  and the impact of loss was relatively less critical than delay on the TCP throughput' is well supported as we can see from
  the graphs.

## Reproducible research



1) Did the authors include instructions for reproducing the experiment in 3 ways (Raw data -> Results,
Existing experiment setup -> Data, and Set up experiment)? Are the instructions clear
and easy to understand?

  Yes, the project folder has everything and the instrucions is clear enough.

2) Were you able to successfully produce experiment results?

  I was able to reproduce the experiment successfully.

3) How long did it take you to run this experiment, from start to finish?

  It takes me about 2 hours since we need to change many settings once and once again.

4) Did you need to make any changes or do any additional steps beyond the documentation in order to successfully complete this experiment? Describe *in detail*. How long did these extra steps or changes take to figure out?

  No, I followed the instructions till the very last.

5) In the [lecture on reproducible experiments](http://witestlab.poly.edu/~ffund/el6383/files/Reproducible+experiments.pdf), we mentioned six degrees of reproducibility. How would you characterize this experiment - where does it fall on the six degrees of reproducibility?

  I consider of 3.

## Other comments to authors

Please write any other comments that you think might help the authors
of this project improve their experiment.

  Reduce one parameter will make this experiment take less time to finish. But general speaking, it's a good experiment.
