
Project Review
=====================================================


## Overview

1) Briefly summarize the experiment in this project.

Answer: The experiment studies the behaviour of throughput for UDP and TCP algorithms using different packet size, by varying the parameters such as latency, bandwidth and packet loss rate.


2) Does the project generally follow the guidelines and parameters we have
learned in class?

Answer: Yes, the project follows the guidlines and parameters that we have learned in the class. The parameters used were latency,bandwidth and packet loss rate.
It is aimed at learning the outcome of the experiment without too many trials.


## Experiment design

1) What is the goal of this experiment? Is it a focused, specific goal?
Is it useful and likely to have interesting results?

Answer: The goal of the experiment was a little shallow as they want to study the **behaviour** of TCP and UDP algorithms. It could have been a little more specific,
like what variations are expected by changing the parameters and doing this experiment to prove those results.


2) Are the metric(s) and parameters chosen appropriate for this
experiment goal? Does the experiment design clearly support the experiment goal?

Answer: The parameters chosen were appropiate, as they are studying the behaviour by varying certain parameters and in the end prove the results for different variations.
Only one parameter packet size was known to be of no use in the experiment, as not much variation was observed changing it.


3) Is the experiment well designed to obtain maximum information with the
minimum number of trials?

Answer:Since, it studies the behaviour of algorithms, they could have made 2 runs of each variation and then taken the mean of the results to see the outcome.

4) Are the metrics selected for study the *right* metrics? Are they clear,
unambiguous, and likely to lead to correct conclusions? Are there other
metrics that might have been better suited for this experiment?

Answer: The metrics chosen are correct and it leads to the appropiate conclusions such as(just one of the conclusions) for large bandwidth and large delay(i.e LFN ) the throughput
decreases as the latency increases.


5) Are the parameters of the experiment meaningful? Are the ranges
over which parameters vary meaningful and representative?

Answer: Yes,all the parameters expcept packet size of the experiment are meaningful in the sense that they have chosen the parameters that produces a change in the metric chosen(throughput).
Varying packet size does not have much change in the behaviour, it could have been avoided.

6) Have the authors sufficiently addressed the possibility of interactions
between parameters?

Answer: They have sufficiently addressed the possibilities of the interaction between parameters in the conclusion, showing what are the effects of each parameter on the other.


7) Are comparisons made reasonably? Is the baseline selected for comparison appropriate
and realistic?

Answer: There is actually no comparison in this experiment as they are trying to study the behaviour of the two algorithms and not comparing it.

## Communicating results


1) Do the authors report the quantitative results of their experiment?

Answer: Yes, the authors have given all the quantitative results of the experiment.

2) Is there information given about the variation and/or distribution of
experimental results?

Answer: No there is no information about the variation or distribution about the experiment results.

3) Do the authors practice *data integrity* - telling the truth about their data,
avoiding ratio games and other practices to artificially make their results seem better?

Answer: Yes, they have maintained the *data integrity*, I performed the experiment setup and found the
results to be approximately the same. They have not used any ratio's and the values or the data is authentic.

4) Is the data presented in a clear and effective way? If the data is presented in
graphical form, is the type of graph selected appropriate for the "story" that
the data is telling?

Answer: Yes, the data is presented in a clear and effective way, the file name clearly indicate the data for what parameter variation, type of algorithm used
and packet size.

5) Are the conclusions drawn by the authors sufficiently supported by the
experiment results?

Answer: Yes, all the conclusion drawn by the author are sufficiently suported by the experimental results, such as
the decrease in throughput for high bandwidth delay networks. All the conclusions are well supported by graphs.

## Reproducible research



1) Did the authors include instructions for reproducing the experiment in 3 ways (Raw data -> Results,
Existing experiment setup -> Data, and Set up experiment)? Are the instructions clear
and easy to understand?

Answer: The author has included the instructions for reproducing the experiment in  all the 3 ways.
They have included the rspec file with which exsisting experiment setup can be be used to produce data, but no link for the current used geni slice was given to login as the guest user.
They have provided the instructions to set up the experiment and using the experiment setup to get the data.
They have provided all the raw data in the repository and R script to generate the results.


2) Were you able to successfully produce experiment results?

Answer: Yes, I was able successfully able to reproduce the results, with results almost matching those given by the author.


3) How long did it take you to run this experiment, from start to finish?

Answer: It took me about 3 hours to complete the experiment from start to finish, after going the report thoroughly at first
and getting to know what is exactly done in the experiment.

4) Did you need to make any changes or do any additional steps beyond the documentation in order to successfully complete this experiment? Describe *in detail*. How long did these extra steps or changes take to figure out?

Answer: Yes, I need to make a few changes. There was some discrepancy in the commands
1. The first command had some typing error, so it took a bit of time to figure it out.
2. On varying the parameters, the author does not mention to vary all the parameters using a single command, rather do it step by step.But when varying them step
by step, the changes were reflected in only one parameter. The other parameter changed back to its default value.
For example: when I changed the bandwidth to 1000Mbps or 1Gbps and tried to change the latency, the bandwidth changed to unlimited.
It took some time for me to figure it out that if I used all the changes in one single command, all the parameters change.

5) In the [lecture on reproducible experiments](http://witestlab.poly.edu/~ffund/el6383/files/Reproducible+experiments.pdf), we mentioned six degrees of reproducibility. How would you characterize this experiment - where does it fall on the six degrees of reproducibility?

Answer: I would rate it as experiment of degree 3, as it requires some effort from the independent researcher to reproduce the experiment.

## Other comments to authors

Please write any other comments that you think might help the authors
of this project improve their experiment.
