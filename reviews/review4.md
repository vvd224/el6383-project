Project Review
=====================================================


## Overview

1) Briefly summarize the experiment in this project.


In this experiment, we will vary the link parameters(bandwidth, latency, packet loss rates) and packet size, in order to figure out their influences

on UDP or TCP and understand the throughput behaviour of these two protocols.


2) Does the project generally follow the guidelines and parameters we have
learned in class?


The goal is clear but not so specific. Too many variable in the expriment, make the goal looks complex.

The chosen parameters and metrics are representative for archiving this goal. No meaningless measurements.

Results are reproducible.



## Experiment design

1) What is the goal of this experiment? Is it a focused, specific goal?
Is it useful and likely to have interesting results?


1.The goal of their experiment is to understand the throughput behaviour of UDP

and TCP by varying packet sizes and link parameters like bandwidth, latency and

packet loss rates.

2.I think it is a complex goal rather than a focused, specific goal.As a focused

one, it should focus on the throughput behavior of either UDP or TCP but not both

of them. Or it can be the expriment to comparing the different throughput behaviour

of UDP and TCP while keep the other parameters consistent.

3.It is likely to have some interesting results.There are some different throughtput

behaviour between UDP AND TCP.And for the UDP or TCP itself, some different throughput

behaviours occured when bandwidth, latency or packet loss rate got changed.


2) Are the metric(s) and parameters chosen appropriate for this
experiment goal? Does the experiment design clearly support the experiment goal?


The metrics and parameters chosen by the authors are appropriate for this expriments goal. However, too many parameters

make the experiment complex.


3) Is the experiment well designed to obtain maximum information with the
minimum number of trials?


Yes. low bandwidth low latency, low bandwidth high latency, high bandwidth low latency,

low bandwidth low latency. Vary packet loss rate on both high bandwidth link and low bandwidth

link. All possibilities are considered.


4) Are the metrics selected for study the *right* metrics? Are they clear,
unambiguous, and likely to lead to correct conclusions? Are there other
metrics that might have been better suited for this experiment?


The gold of this experiment is to understand the throughput behaviour of UDP and TCP.

So the authors choose the throughput as their metric is reasonable.


5) Are the parameters of the experiment meaningful? Are the ranges
over which parameters vary meaningful and representative?


Yes the parameters of the experiment is meaningful. However, the ranges over packet loss rate is too small.

For 100Mbit/s Bandwidth, the throughput of TCP shows no different when packet loss rate increase from 0.0001 to 0.001, but it drop obviously when

packet loss rate increase from 0.001 to 0.005.


6) Have the authors sufficiently addressed the possibility of interactions
between parameters?


Yes. The author vary one parameter when keep the other parameters constant to address the possibility of interactions between parameters.


7) Are comparisons made reasonably? Is the baseline selected for comparison appropriate
and realistic?


The authors made a reasonable comparison. the throughput performance of UDP and TCP and The influence of packet size and packet loss rate on UDP or TCP are compared in each plot.

While, the influences of bandwidth and latency on UDP or TCP can be drawn when two plots when only one parameters get changed.



## Communicating results


1) Do the authors report the quantitative results of their experiment?


Yes. Eight plots are created from the raw data collected by R. Six plots show the values of throughput in six conditions. Two plots show the confidence interval for the throughput

of TCP and UDP.


2) Is there information given about the variation and/or distribution of
experimental results?


The authors just draw a conclusion from the plots but give no explanation about

the variation or distribution of experimental results.


3) Do the authors practice *data integrity* - telling the truth about their data,
avoiding ratio games and other practices to artificially make their results seem better?


Yes, those data are collected by author from his expriment. However, the data I got has a little differences between the one got by author. The value

of thoughput vary a lot under some specific condition, so more trials have to be done.


4) Is the data presented in a clear and effective way? If the data is presented in
graphical form, is the type of graph selected appropriate for the "story" that
the data is telling?


No, the data do not presented in a clear and effective way. the line graph and bar graph are being used.

However, the value of the throughput is too large, so the exact value can not be display accurately. Due

to the tremendous differiences between the TCP and UDP, variation of UDP throughput values can not be

observed.(always a straight line)


5) Are the conclusions drawn by the authors sufficiently supported by the
experiment results?


Yes.The conclusions drawn by the authors sufficiently supported by the experiment results.

The author created 8 plots from the raw data they got by R. They draw their conclusions by

observe each plot and compare some of these plots.


## Reproducible research



1) Did the authors include instructions for reproducing the experiment in 3 ways (Raw data -> Results,
Existing experiment setup -> Data, and Set up experiment)? Are the instructions clear
and easy to understand?


Yes. the authors include instructions for reproducing the experiment in 3 ways. However, The arragement

of setup make me confusing and took me some times to figure out what the author want to say.


2) Were you able to successfully produce experiment results?


Yes. the results I get is similar to the author.


3) How long did it take you to run this experiment, from start to finish?


3 - 4 hours.


4) Did you need to make any changes or do any additional steps beyond the documentation in order to successfully complete this experiment? Describe *in detail*. How long did these extra steps or changes take to figure out?


No,I don't need to make any changes. However, some of the command in the instruction is not work.

For command "sudo ipfw pipe 60111 config bw 100M", 100M should be substituted by 100000000

For command "iperf -s", the -u should be added for UDP transmission.

For commands "sudo ipfw pipe 60111 config bw 100000000", "sudo ipfw pipe 60111 config delay 50" , "sudo ipfw pipe 60111 config plr .0001", they should execute as one command as "sudo

ipfw pipe 60111 config bw 100000000 delay 50 plr .0001".


5) In the [lecture on reproducible experiments](http://witestlab.poly.edu/~ffund/el6383/files/Reproducible+experiments.pdf), we mentioned six degrees of reproducibility. How would you characterize this experiment - where does it fall on the six degrees of reproducibility?


it fall on the third of reproucibility


## Other comments to authors

Please write any other comments that you think might help the authors
of this project improve their experiment.


The project report is pretty good. However, More trials and clearer plots are needed.
